#include <atomic>
#include <thread>
#include <chrono>
#include <iostream>

template <typename Resolution>
class Timer {
    uint64_t _start;

public:
    Timer() { _start = now(); }

    uint64_t now() {
        return std::chrono::duration_cast<Resolution>(
            std::chrono::high_resolution_clock::now().time_since_epoch())
            .count();
    }

    void start() { _start = now(); }
    uint64_t stop() { return now() - _start; }
    uint64_t peek() { return now() - _start; }
};

#define ALWAYS_INLINE __attribute__((always_inline))
#define CACHELINE_SIZE 64
#define CACHE_ALIGN alignas(CACHELINE_SIZE)

class SpinlockTas {
public:
    ALWAYS_INLINE void lock() {
        while (_locked.exchange(true, std::memory_order_acquire) == true);        
    }

    ALWAYS_INLINE void unlock() {
        _locked.store(false, std::memory_order_release);
    }

private:
    CACHE_ALIGN std::atomic_bool _locked = {false};
};

static_assert(sizeof(SpinlockTas) == CACHELINE_SIZE, "SpinlockTas should be the size of a cache line");

std::mutex _mutex;

int main() {

    std::atomic_bool is_running(true);
    std::thread t1([&]() {
            // tight loop
            Timer <std::chrono::milliseconds> outer_timer;
            uint64_t outer_counter = 0;
            uint64_t counter = 0;
            uint64_t time_waiting = 0;
            while(outer_counter < 5000) {
                Timer <std::chrono::microseconds> inner_timer;
                inner_timer.start();
                Timer <std::chrono::microseconds> lock_timer;
                std::lock_guard<std::mutex> lock(_mutex);
                time_waiting += lock_timer.stop();
                while (inner_timer.peek() < 1000) {
                    counter++;
                }
                outer_counter++;
            }
            std::cerr << "outer took " << outer_timer.stop() << "ms" << std::endl;
            std::cerr << "[t1] spent " << time_waiting << "us waiting on lock" << std::endl;
            is_running = false;
    });

    std::thread t2([&]() {
            uint64_t time_waiting = 0;
            while (is_running) {
                std::this_thread::sleep_for(std::chrono::milliseconds(rand() % 100));
                Timer<std::chrono::microseconds> timer;
                std::lock_guard<std::mutex> lock(_mutex);
                time_waiting += timer.stop();
            }
            std::cerr << "[t2] spent " << time_waiting << "us waiting on lock" << std::endl;
    });

    t1.join();
    t2.join();

    return 0;
}
